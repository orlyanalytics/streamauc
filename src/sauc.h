#ifndef SAUC_H
#define SAUC_H

#include "rbtree.h"
#include "queue.h"
#include <stdint.h>
#include <stdlib.h>
#include <limits>



struct aucnode
{
	double score;

	struct stats {
		double pos, neg;

		stats() : pos(0), neg(0) {}

		stats &
		operator += (const struct stats & a)
		{
			pos += a.pos;
			neg += a.neg;

			return *this;
		}
		
		stats &
		operator -= (const struct stats & a)
		{
			pos -= a.pos;
			neg -= a.neg;

			return *this;
		}



		stats 
		operator - (const struct stats & a) const
		{
			stats ret;
			ret.pos = pos - a.pos;
			ret.neg = neg - a.neg;
			return ret;
		}



	} lab, acc, gap, auc;


	rblinks<aucnode> links;
	rblinks<aucnode> poslinks;

	TAILQ_ENTRY(aucnode) pos, comp;
};

TAILQ_HEAD(auchead, aucnode);


struct score_key
{
	double operator()(aucnode *p)  {return p->score;}
};

struct links_getter
{
	rblinks<aucnode> *operator ()(aucnode *p) {return &p->links;}
};

struct poslinks_getter
{
	rblinks<aucnode> *operator ()(aucnode *p) {return &p->poslinks;}
};

struct accumulator
{
	void
	operator()(aucnode *n)
	{
		n->acc = n->lab;
		if (n->links.left) n->acc += n->links.left->acc;
		if (n->links.right) n->acc += n->links.right->acc;
	}
};

struct null_update
{
	void operator()(aucnode *) {}
};


typedef rbtree<aucnode, score_key, links_getter, accumulator> searchtree;
typedef rbtree<aucnode, score_key, poslinks_getter, null_update> postree;

class sauc
{
	public:
		sauc (double alpha) :
			m_tree(score_key(), links_getter(), accumulator()),
			m_postree(score_key(), poslinks_getter(), null_update()),
			m_alpha(alpha), m_list_size(0)
		{
			TAILQ_INIT(&m_positives);
			TAILQ_INIT(&m_compressed);

			m_right.score = std::numeric_limits<double>::infinity();
			m_left.score = -m_right.score;

			m_tree.insert(&m_left);
			m_tree.insert(&m_right);

			m_postree.insert(&m_left);
			m_postree.insert(&m_right);

			TAILQ_INSERT_HEAD(&m_positives, &m_right, pos);
			TAILQ_INSERT_HEAD(&m_positives, &m_left, pos);

			TAILQ_INSERT_HEAD(&m_compressed, &m_right, comp);
			TAILQ_INSERT_HEAD(&m_compressed, &m_left, comp);
		}

		void insert(double score, bool label) {if (label) insert_positive(score); else insert_negative(score);}
		void remove(double score, bool label) {if (label) remove_positive(score); else remove_negative(score);}

		double estimate();

		uint32_t list_size() const {return m_list_size;}

	protected:
		void insert_negative(double score);
		void remove_negative(double score);
		
		void insert_positive(double score);
		void remove_positive(double score);

		aucnode *insert_tree_positive(double score);
		void remove_tree_positive(aucnode *n);

		aucnode *find_compressed(double score);
		aucnode::stats head_stats(aucnode *v);

		void add_next(aucnode *n);
		void compress();

		searchtree m_tree;
		postree m_postree;
		auchead m_positives;
		auchead m_compressed;
		double m_alpha;

		aucnode m_left, m_right; // sentinels
		uint32_t m_list_size;
};


#endif
