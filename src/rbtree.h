#ifndef RBTREE_H
#define RBTREE_H

enum ctype {BLACK, RED};
#define LEAF 0

#include <stdio.h>

template<class T>
struct rblinks
{
	T *parent;
	T *left;
	T *right;
	ctype color;
};

template<class T, typename key, typename links, typename update>
class rbtree
{
	public:
		rbtree(key comp, links l, update u) : m_key(comp), m_links(l), m_update(u), m_root(0) {}

		void insert(T *n);
		void remove(T *n);
		T * find(double k);

		void refresh(T *n);

		T * root() {return m_root;}
		T *find_min(T *n);

	protected:
		typedef rblinks<T> links_t;


		void insert_recurse(T *n);
		void insert_repair_tree(T *n);

		void insert_case1(T *n);
		void insert_case2(T *n);
		void insert_case3(T *n);
		void insert_case4(T *n);

		void delete_one_child(T *n);
		void delete_case1(T *n);
		void delete_case2(T *n);
		void delete_case3(T *n);
		void delete_case4(T *n);
		void delete_case5(T *n);
		void delete_case6(T *n);

		inline T *left(T *n) {return m_links(n)->left;}
		inline T *right(T *n) {return m_links(n)->right;}
		inline ctype color(T *n) {return m_links(n)->color;}

		void set_color(T *n, ctype c) {m_links(n)->color = c;}
		void set_left(T *n, T *c) {m_links(n)->left = c;}
		void set_right(T *n, T *c) {m_links(n)->right = c;}
		void set_parent(T *n, T *p) {m_links(n)->parent = p;}

		void rotate_left(T *n);
		void rotate_right(T *n);

		void swap(T *n, T *m);
		void attach(T *n, T *p, bool lc, T *a, T *b);

		inline T *
		parent(T *n)
		{
			if (n) return m_links(n)->parent;
			else return 0;
		}


		inline T *
		sibling(T *n)
		{
			T *p = parent(n);
			if (p == 0) return 0; // No parent means no sibling
			if (n == left(p)) return right(p);
			else return left(p);
		}

		inline T * grandparent(T *n) {return parent(parent(n));}
		inline T * uncle(T *n) {return sibling(parent(n));}

		key m_key;
		links m_links;
		update m_update;
		T *m_root;
};

template<class T, class key, typename links, typename update>
T *
rbtree<T, key, links, update>::find(double k)
{
	T *n = m_root;
	T *ret = 0;

	while (n) {
		//printf("key: %f %f %p\n", m_key(n), k, left(n));
		if (m_key(n) == k)
			return n;
		if (m_key(n) > k)
			n = left(n);
		else if (m_key(n) < k) {
			if (ret == 0 || m_key(ret) < m_key(n))	
				ret = n;
			n = right(n);
		}
	}

	return ret;
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::refresh(T *n)
{
	while (n) {
		m_update(n);
		n = parent(n);
	}
}


template<class T, class key, typename links, typename update>
T *
rbtree<T, key, links, update>::find_min(T *n)
{
	while (left(n)) {
		n = left(n);
	}
	return n;
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::insert(T *n)
{
	// insert new node into the current tree
	insert_recurse(n);

	refresh(n);

	// repair the tree in case any of the red-black properties have been violated
	insert_repair_tree(n);

	// find the new root 
	m_root = n;
	while (parent(m_root))
		m_root = parent(m_root);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::remove(T *n)
{
	if (left(n) && right(n)) {
		T *m = find_min(right(n));
		swap(n, m);
		refresh(n);
		refresh(m);
		//printf("SWAP\n");
	}
	delete_one_child(n);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::insert_recurse(T *n)
{
	T *root = m_root;

	// recursively descend the tree until a leaf is found
	while (root) {
		if (m_key(n) < m_key(root)) {
			if (left(root) != LEAF)
				root = left(root);
			else {
				set_left(root, n);
				break;
			}
		}
		else {
			if (right(root) != LEAF)
				root = right(root);
			else {
				set_right(root, n);
				break;
			}
		}
	}

	set_parent(n, root);
	set_left(n, LEAF);
	set_right(n, LEAF);
	set_color(n, RED);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::
insert_repair_tree(T *n)
{
	if (parent(n) == 0)
		set_color(n, BLACK);
	else if (color(parent(n)) == BLACK) {}
	else if (uncle(n) && color(uncle(n)) == RED) 
		insert_case3(n);
	else 
		insert_case4(n);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::insert_case2(T *)
{
	return; /* Do nothing since tree is still valid */
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::insert_case3(T *n)
{
	set_color(parent(n), BLACK);
	set_color(uncle(n), BLACK);
	set_color(grandparent(n), RED);
	insert_repair_tree(grandparent(n));
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::insert_case4(T *n)
{
	T *p = parent(n);
	T *g = grandparent(n);

	if (left(g) && n == right(left(g))) {
		rotate_left(p);
		n = left(n);
	}
	else if (right(g) && n == left(right(g))) {
		rotate_right(p);
		n = right(n); 
	}

	p = parent(n);
	g = grandparent(n);

	if (n == left(p))
		rotate_right(g);
	else
		rotate_left(g);
	set_color(p, BLACK);
	set_color(g, RED);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::rotate_left(T *p)
{
	T *n = right(p);
	T *b = left(n);
	T *g = parent(p);

	set_right(p, b);
	if (b) set_parent(b, p);

	set_left(n, p);
	set_parent(p, n);

	set_parent(n, g);
	if (g) {
		if (left(g) == p) set_left(g, n);
		else if (right(g) == p) set_right(g, n);
	}
	else
		m_root = n;

	m_update(p);
	m_update(n);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::rotate_right(T *p)
{
	T *n = left(p);
	T *b = right(n);
	T *g = parent(p);

	set_left(p, b);
	if (b) set_parent(b, p);

	set_right(n, p);
	set_parent(p, n);

	set_parent(n, g);
	if (g) {
		if (left(g) == p) set_left(g, n);
		else if (right(g) == p) set_right(g, n);
	}
	else
		m_root = n;

	m_update(p);
	m_update(n);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::swap(T *n, T *m)
{
	if (parent(n) == m) {
		T *t;
		t = n; n = m; m = t;
	}

	T *a1 = left(n);
	T *b1 = right(n);
	T *p1 = parent(n);
	bool l1 = p1 && left(p1) == n;

	T *a2 = left(m);
	T *b2 = right(m);
	T *p2 = parent(m);
	bool l2 = p2 && left(p2) == m;

	if (b1 == m) {
		attach(m, p1, l1, a1, n);
		attach(n, m, false, a2, b2);
	}
	else if (a1 == m) {
		attach(m, p1, l1, n, b1);
		attach(n, m, true, a2, b2);
	}
	else {
		attach(m, p1, l1, a1, b1);
		attach(n, p2, l2, a2, b2);
	}

	ctype c = color(n);
	set_color(n, color(m));
	set_color(m, c);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::attach(T *n, T *p, bool lc, T *a, T *b)
{
	set_left(n, a);
	set_right(n, b);
	set_parent(n, p);
	if (a) set_parent(a, n);
	if (b) set_parent(b, n);
	if (p) {
		if (lc) set_left(p, n);
		else set_right(p, n);
	}
	else
		m_root = n;
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_one_child(T *n)
{
	T *child = left(n) ? left(n) : right(n);

	if (color(n) == BLACK) {
		if (child != LEAF)
			set_color(child, BLACK); // child's color must be red since n has only one child
		else
			delete_case1(n); // will only happen if child == LEAF
	}

	// replace n with child
	T *p = parent(n);
	if (child != LEAF) set_parent(child, p);

	if (p) {
		if (left(p) == n) set_left(p, child);
		else set_right(p, child);
		refresh(p);
	}
	else
		m_root = child;
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case1(T *n)
{
	if (parent(n)) delete_case2(n);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case2(T *n)
{
	T *s = sibling(n);
	T *p = parent(n);

	if (color(s) == RED) {
		set_color(p, RED);
		set_color(s, BLACK);
		if (n == left(p))
			rotate_left(p);
		else
			rotate_right(p);
	}
	delete_case3(n);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case3(T *n)
{
	T *s = sibling(n);
	T *p = parent(n);

	if ((color(p) == BLACK) && (color(s) == BLACK) && // s != LEAF because n->parent->color == BLACK
		(left(s) == LEAF || color(left(s)) == BLACK) &&
		(right(s) == LEAF || color(right(s)) == BLACK)) {
		set_color(s, RED);
		delete_case1(p);
	}
	else
		delete_case4(n);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case4(T *n)
{
	T *s = sibling(n);
	T *p = parent(n);

	if ((color(p) == RED) && (color(s) == BLACK) &&
		(left(s) == LEAF || color(left(s)) == BLACK) &&
		(right(s) == LEAF || color(right(s)) == BLACK)) {
		set_color(s, RED);
		set_color(p, BLACK);
	}
	else
		delete_case5(n);
}

template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case5(T *n)
{
	T *s = sibling(n);
	T *p = parent(n);

	if ((n == left(p)) && (right(s) == LEAF || color(right(s)) == BLACK)) { 
		set_color(s, RED);
		set_color(left(s), BLACK);
		rotate_right(s);
	}
	else if ((n == right(p)) && (left(s) == LEAF || color(left(s)) == BLACK)) {
		set_color(s, RED);
		set_color(right(s), BLACK);
		rotate_left(s);
	}
	delete_case6(n);
}


template<class T, class key, typename links, typename update>
void
rbtree<T, key, links, update>::delete_case6(T *n)
{
	T *s = sibling(n);
	T *p = parent(n);

	set_color(s, color(p));
	set_color(p, BLACK);

	if (n == left(p)) {
		set_color(right(s), BLACK);
		rotate_left(p);
	}
	else {
		set_color(left(s), BLACK);
		rotate_right(p);
	}
}


#endif

#if 0
struct node
{
	double key;
	node *parent;
	node *left;
	node *right;
	ctype color;
};
#include "rbtree.h"
#include <stdlib.h>

void insert_case1(struct node *n);
void insert_case2(struct node *n);
void insert_case3(struct node *n);
void insert_case4(struct node *n);
void insert_case4step2(struct node *n);

void rotate_left(struct node *n);
void rotate_right(struct node *n);

void insert_repair_tree(struct node *n);
void insert_recurse(struct node *root, struct node *n);

struct node * parent(struct node *n);
struct node * grandparent(struct node *n);
struct node * uncle(struct node *n);
struct node * sibling(struct node *n);

void delete_case1(struct node *n);
void delete_case2(struct node *n);
void delete_case3(struct node *n);
void delete_case4(struct node *n);
void delete_case5(struct node *n);
void delete_case6(struct node *n);
void delete_one_child(struct node *n);


struct node *
insert(struct node *root, struct node *n)
{
	// insert new node into the current tree
	insert_recurse(root, n);

	// repair the tree in case any of the red-black properties have been violated
	insert_repair_tree(n);

	// find the new root to return
	root = n;
	while (parent(root) != NULL)
		root = parent(root);
	return root;
}


void
insert_recurse(struct node *root, struct node *n){
	// recursively descend the tree until a leaf is found
	if (root != NULL && n->key < root->key) {
		if (root->left != LEAF) {
			insert_recurse(root->left, n);
			return;
		}
		else
			root->left = n;
	}
	else if (root != NULL) {
		if (root->right != LEAF){
			insert_recurse(root->right, n);
			return;
		}
		else
			root->right = n;
	}

	// insert new node n
	n->parent = root;
	n->left = LEAF;
	n->right = LEAF;
	n->color = RED;
}


void
insert_repair_tree(struct node *n)
{
	if (parent(n) == NULL) {
		insert_case1(n);
	}
	else if (parent(n)->color == BLACK) {
		insert_case2(n);
	}
	else if (uncle(n)->color == RED) {
		insert_case3(n);
	}
	else {
		insert_case4(n);
	}
}


struct node *
parent(struct node *n)
{
	if (n != NULL)
		return n->parent;
	else
		return NULL;
}



struct node *
grandparent(struct node *n)
{
	if ((n != NULL) && (n->parent != NULL))
		return n->parent->parent;
	else
		return NULL;
}


struct node *
sibling(struct node *n)
{
	struct node *p = parent(n);
	if (p == NULL)
		return NULL; // No parent means no sibling
	if (n == p->left)
		return p->right;
	else
		return p->left;
}


struct node *
uncle(struct node *n)
{
	struct node *g = grandparent(n);
	if (g == NULL)
		return NULL; // No grandparent means no uncle

	if (n->parent == g->left)
		return g->right;
	else
		return g->left;
}


void
rotate_left(struct node *n)
{
	struct node *b = n->left;
	struct node *p = n->parent;
	//struct node *a = p->left;
	struct node *g = p->parent;

	p->right = b;
	if (b != NULL) b->parent = p;

	n->left = p;
	p->parent = n;

	n->parent = g;
	if (g != NULL) {
		if (g->left == p) g->left = n;
		else if (g->right == p) g->right = n;
	}
}

void
rotate_right(struct node *n)
{
	struct node *b = n->right;
	struct node *q = n->parent;
	//struct node *a = p->left;
	struct node *g = q->parent;

	q->right = b;
	if (b != NULL) b->parent = q;

	n->right = q;
	q->parent = n;

	n->parent = g;
	if (g != NULL) {
		if (g->left == q) g->left = n;
		else if (g->right == q) g->right = n;
	}
}

void insert_case1(struct node *n)
{
	if (parent(n) == NULL)
		n->color = BLACK;
}

void insert_case2(struct node *n)
{
	return; /* Do nothing since tree is still valid */
}

void insert_case3(struct node *n)
{
	parent(n)->color = BLACK;
	uncle(n)->color = BLACK;
	grandparent(n)->color = RED;
	insert_repair_tree(grandparent(n));
}

void insert_case4(struct node *n)
{
	struct node *p = parent(n);
	struct node *g = grandparent(n);

	if (n == g->left->right) {
		rotate_left(p);
		n = n->left;
	}
	else if (n == g->right->left) {
		rotate_right(p);
		n = n->right; 
	}

	insert_case4step2(n);
}


void insert_case4step2(struct node *n)
{
	struct node *p = parent(n);
	struct node *g = grandparent(n);

	if (n == p->left)
		rotate_right(g);
	else
		rotate_left(g);
	p->color = BLACK;
	g->color = RED;
}

void
replace_node(struct node *n, struct node *child)
{
}

void delete_one_child(struct node *n)
{
	struct node *child = n->right == LEAF ? n->left : n->right;

	if (n->color == BLACK) {
		if (child != LEAF)
			child->color = BLACK; // child's color must be red since n has only one child
		else
			delete_case1(n); // will only happen if child == LEAF
	}
	replace_node(n, child);
}

void
delete_case1(struct node *n)
{
	if (n->parent != NULL) delete_case2(n);
}


void
delete_case2(struct node *n)
{
	struct node *s = sibling(n);

	if (s->color == RED) {
		n->parent->color = RED;
		s->color = BLACK;
		if (n == n->parent->left)
			rotate_left(n->parent);
		else
			rotate_right(n->parent);
	}
	delete_case3(n);
}


void
delete_case3(struct node *n)
{
	struct node *s = sibling(n);

	if ((n->parent->color == BLACK) &&
		(s->color == BLACK) && // s != LEAF because n->parent->color == BLACK
		(s->left == LEAF || s->left->color == BLACK) &&
		(s->right == LEAF || s->right->color == BLACK)) {
		s->color = RED;
		delete_case1(n->parent);
	}
	else
		delete_case4(n);
}

void
delete_case4(struct node *n)
{
	struct node *s = sibling(n);

	if ((n->parent->color == RED) &&
		(s->color == BLACK) &&
		(s->left == LEAF || s->left->color == BLACK) &&
		(s->right == LEAF || s->right->color == BLACK)) {
		s->color = RED;
		n->parent->color = BLACK;
	}
	else
		delete_case5(n);
}

void
delete_case5(struct node *n)
{
	struct node *s = sibling(n);

	if ((n == n->parent->left) && (s->right == LEAF || s->right->color == BLACK)) { 
		s->color = RED;
		s->left->color = BLACK;
		rotate_right(s);
	}
	else if ((n == n->parent->right) && (s->left == LEAF || s->left->color == BLACK)) {
		s->color = RED;
		s->right->color = BLACK;
		rotate_left(s);
	}
	delete_case6(n);
}


void
delete_case6(struct node *n)
{
	struct node *s = sibling(n);

	s->color = n->parent->color;
	n->parent->color = BLACK;

	if (n == n->parent->left) {
		s->right->color = BLACK;
		rotate_left(n->parent);
	}
	else {
		s->left->color = BLACK;
		rotate_right(n->parent);
	}
}
#endif
