#include "sauc.h"
#include <assert.h>
#include <stdio.h>

double
sauc::estimate()
{
	double a = 0;
	aucnode::stats cum;

	aucnode *v;


	TAILQ_FOREACH(v, &m_compressed, comp) {
		//printf("foo %f %f %f %f %f %f at %p score = %f\n", v->lab.pos, v->lab.neg, v->auc.pos, v->auc.neg, v->gap.pos, v->gap.neg, v, v->score);
		assert(v->lab.pos >= 0);
		assert(v->lab.neg >= 0);
		assert(v->auc.pos >= v->lab.pos);
		assert(v->auc.neg >= v->lab.neg);
		assert(v->auc.pos >= v->gap.pos);
		assert(v->auc.neg >= v->gap.neg);

		aucnode::stats s = v->lab;
		a += (cum.pos + s.pos / 2) * s.neg;
		cum += s;
		s = v->auc - v->lab;
		a += (cum.pos + s.pos / 2) * s.neg;
		cum += s;
	}

	
	if (cum.pos == 0 || cum.neg == 0)
		return 1;
	
	//printf("%f %f\n", m_tree.root()->acc.pos, cum.pos);

	assert(cum.pos == m_tree.root()->acc.pos);
	assert(cum.neg == m_tree.root()->acc.neg);

	return a / (cum.pos * cum.neg);
}

void
sauc::remove_negative(double score)
{
	aucnode *n = m_tree.find(score);
	//printf("removing negative node %p %f\n", n, score);
	assert(n->score == score);
	assert(n->lab.neg > 0);


	n->lab.neg--;
	m_tree.refresh(n);

	aucnode *p = m_postree.find(score);
	p->gap.neg--;

	p = find_compressed(score);
	p->auc.neg--;

	if (n->lab.neg == 0 && n->lab.pos == 0) {
		m_tree.remove(n);
		delete n;
	}
}

void
sauc::insert_negative(double score)
{
	//printf("foo\n");

	aucnode *n = m_tree.find(score);
	assert(n);


	if (n->score != score) {
		n = new aucnode;
		n->score = score;
		m_tree.insert(n);
	}

	//printf("inserting negative node %p\n", n);

	n->lab.neg++;
	m_tree.refresh(n);


	aucnode *p = m_postree.find(score);

	//printf("foo %p\n", p);

	p->gap.neg++;

	//printf("foo\n");

	p = find_compressed(score);
	p->auc.neg++;

	//printf("foo\n");
}

void
sauc::insert_positive(double score)
{
	insert_tree_positive(score);
	aucnode *u = find_compressed(score);	
	u->auc.pos++;
	add_next(u);
	compress();
}

void
sauc::remove_positive(double score)
{
	aucnode *v = m_tree.find(score);
	aucnode *u = find_compressed(score);
	//assert(v->lab.pos > 0);

	////printf("min: ", m_postree.find_min(m_postree.root));

	//printf("removing positive %p %p %f %f\n", u, v, v->lab.pos, score);

	if (u == v && v->lab.pos == 1) {
		add_next(u);
		aucnode *w = TAILQ_PREV(u, auchead, comp);
		w->auc += u->auc;
		w->auc.pos--;
		TAILQ_REMOVE(&m_compressed, u, comp);
		m_list_size--;
	}
	else
		u->auc.pos--;

	remove_tree_positive(v);

	double c = 0;
	aucnode *r = TAILQ_FIRST(&m_compressed);
	aucnode *w = TAILQ_NEXT(r, comp);

	while (w) {
		double x = r->auc.pos;

		if (c + x > m_alpha*(c + r->lab.pos))
			add_next(r);

		c += x;
		r = w;
		w = TAILQ_NEXT(r, comp);
	}


	compress();
}



aucnode *
sauc::insert_tree_positive(double score)
{
	aucnode *v = m_tree.find(score);

	if (v->score == score) {
		v->lab.pos++;
		m_tree.refresh(v);
	}
	else  {
		v = new aucnode;
		v->score = score;
		v->lab.pos = 1;
		m_tree.insert(v);
	}

	//printf("inserting positive node: %p\n", v);

	aucnode *w = m_postree.find(score);
	w->gap.pos++;

	if (v != w) {
		m_postree.insert(v);

		aucnode::stats foo = head_stats(v);
		//printf("v heads: %f, %f %f\n", foo.pos, foo.neg, v->score);
		foo = head_stats(w);
		//printf("w heads: %f, %f %f\n", foo.pos, foo.neg, w->score);
		//printf("total: %f, %f\n", m_tree.root()->acc.pos, m_tree.root()->acc.neg);

		TAILQ_INSERT_AFTER(&m_positives, w, v, pos);
		v->gap = w->gap;
		w->gap = head_stats(v) - head_stats(w);
		v->gap -= w->gap;

		assert(w->gap.pos == w->lab.pos);

		//printf("%f %f\n", w->gap.pos, w->gap.neg);
	}

	return v;
}

void
sauc::remove_tree_positive(aucnode *v)
{
	assert(v != &m_left || v != &m_right);

	v->gap.pos--;

	if (v->lab.pos == 1) {
		aucnode *w = TAILQ_PREV(v, auchead, pos);
		w->gap += v->gap;
		TAILQ_REMOVE(&m_positives, v, pos);
		m_postree.remove(v);
	}

	if (v->lab.pos == 1 && v->lab.neg == 0) {
		m_tree.remove(v);
		delete v;
	}
	else {
		v->lab.pos--;
		m_tree.refresh(v);
	}
}


void
sauc::add_next(aucnode *v)
{
	aucnode *w = TAILQ_NEXT(v, pos);
	if (w == TAILQ_NEXT(v, comp)) return;

	//printf("%f %f %f, %p\n", v->gap.pos, w->auc.pos, v->auc.pos, w);

	TAILQ_INSERT_AFTER(&m_compressed, v, w, comp);
	m_list_size++;
	w->auc = v->auc - v->gap;
	v->auc = v->gap;

	//printf("%f\n", w->auc.pos);
}

void
sauc::compress()
{
	aucnode *v = TAILQ_FIRST(&m_compressed);
	aucnode *w = TAILQ_NEXT(v, comp);
	aucnode *u = TAILQ_NEXT(w, comp);
	double c = 0;

	while (u) {
		if (c + v->auc.pos + w->auc.pos <= m_alpha*(c + v->lab.pos)) {
			v->auc += w->auc;
			TAILQ_REMOVE(&m_compressed, w, comp);
			m_list_size--;
		}
		else {
			c += v->gap.pos;
			v = w;
		}
		w = u;
		u = TAILQ_NEXT(u, comp);
	}
}


aucnode *
sauc::find_compressed(double score)
{
	aucnode *n;
	TAILQ_FOREACH_REVERSE(n, &m_compressed, auchead, comp) {
		if (n->score <= score)
			return n;
	}

	return 0;
}

aucnode::stats
sauc::head_stats(aucnode *v)
{
	aucnode::stats ret;
	aucnode *w = m_tree.root();

	while (true) {
		assert(w);
		//printf("%f %f\n", w->score, v->score);
		if (w->score > v->score)
			w = w->links.left;
		else {
			if (w->links.left) {
				ret += w->links.left->acc;
			}
			if (w == v)
				return ret;
			else {
				ret += w->lab;
				w = w->links.right;
			}
		}
	}
}
