#include "rbtree.h"
#include <stdlib.h>

void insert_case1(struct node *n);
void insert_case2(struct node *n);
void insert_case3(struct node *n);
void insert_case4(struct node *n);
void insert_case4step2(struct node *n);

void rotate_left(struct node *n);
void rotate_right(struct node *n);

void insert_repair_tree(struct node *n);
void insert_recurse(struct node *root, struct node *n);

struct node * parent(struct node *n);
struct node * grandparent(struct node *n);
struct node * uncle(struct node *n);
struct node * sibling(struct node *n);

void delete_case1(struct node *n);
void delete_case2(struct node *n);
void delete_case3(struct node *n);
void delete_case4(struct node *n);
void delete_case5(struct node *n);
void delete_case6(struct node *n);
void delete_one_child(struct node *n);


struct node *
insert(struct node *root, struct node *n)
{
	// insert new node into the current tree
	insert_recurse(root, n);

	// repair the tree in case any of the red-black properties have been violated
	insert_repair_tree(n);

	// find the new root to return
	root = n;
	while (parent(root) != NULL)
		root = parent(root);
	return root;
}


void
insert_recurse(struct node *root, struct node *n){
	// recursively descend the tree until a leaf is found
	if (root != NULL && n->key < root->key) {
		if (root->left != LEAF) {
			insert_recurse(root->left, n);
			return;
		}
		else
			root->left = n;
	}
	else if (root != NULL) {
		if (root->right != LEAF){
			insert_recurse(root->right, n);
			return;
		}
		else
			root->right = n;
	}

	// insert new node n
	n->parent = root;
	n->left = LEAF;
	n->right = LEAF;
	n->color = RED;
}


void
insert_repair_tree(struct node *n)
{
	if (parent(n) == NULL) {
		insert_case1(n);
	}
	else if (parent(n)->color == BLACK) {
		insert_case2(n);
	}
	else if (uncle(n)->color == RED) {
		insert_case3(n);
	}
	else {
		insert_case4(n);
	}
}


struct node *
parent(struct node *n)
{
	if (n != NULL)
		return n->parent;
	else
		return NULL;
}



struct node *
grandparent(struct node *n)
{
	if ((n != NULL) && (n->parent != NULL))
		return n->parent->parent;
	else
		return NULL;
}


struct node *
sibling(struct node *n)
{
	struct node *p = parent(n);
	if (p == NULL)
		return NULL; // No parent means no sibling
	if (n == p->left)
		return p->right;
	else
		return p->left;
}


struct node *
uncle(struct node *n)
{
	struct node *g = grandparent(n);
	if (g == NULL)
		return NULL; // No grandparent means no uncle

	if (n->parent == g->left)
		return g->right;
	else
		return g->left;
}


void
rotate_left(struct node *n)
{
	struct node *b = n->left;
	struct node *p = n->parent;
	//struct node *a = p->left;
	struct node *g = p->parent;

	p->right = b;
	if (b != NULL) b->parent = p;

	n->left = p;
	p->parent = n;

	n->parent = g;
	if (g != NULL) {
		if (g->left == p) g->left = n;
		else if (g->right == p) g->right = n;
	}
}

void
rotate_right(struct node *n)
{
	struct node *b = n->right;
	struct node *q = n->parent;
	//struct node *a = p->left;
	struct node *g = q->parent;

	q->right = b;
	if (b != NULL) b->parent = q;

	n->right = q;
	q->parent = n;

	n->parent = g;
	if (g != NULL) {
		if (g->left == q) g->left = n;
		else if (g->right == q) g->right = n;
	}
}

void insert_case1(struct node *n)
{
	if (parent(n) == NULL)
		n->color = BLACK;
}

void insert_case2(struct node *n)
{
	return; /* Do nothing since tree is still valid */
}

void insert_case3(struct node *n)
{
	parent(n)->color = BLACK;
	uncle(n)->color = BLACK;
	grandparent(n)->color = RED;
	insert_repair_tree(grandparent(n));
}

void insert_case4(struct node *n)
{
	struct node *p = parent(n);
	struct node *g = grandparent(n);

	if (n == g->left->right) {
		rotate_left(p);
		n = n->left;
	}
	else if (n == g->right->left) {
		rotate_right(p);
		n = n->right; 
	}

	insert_case4step2(n);
}


void insert_case4step2(struct node *n)
{
	struct node *p = parent(n);
	struct node *g = grandparent(n);

	if (n == p->left)
		rotate_right(g);
	else
		rotate_left(g);
	p->color = BLACK;
	g->color = RED;
}

void
replace_node(struct node *n, struct node *child)
{
}

void delete_one_child(struct node *n)
{
	struct node *child = n->right == LEAF ? n->left : n->right;

	if (n->color == BLACK) {
		if (child != LEAF)
			child->color = BLACK; // child's color must be red since n has only one child
		else
			delete_case1(n); // will only happen if child == LEAF
	}
	replace_node(n, child);
}

void
delete_case1(struct node *n)
{
	if (n->parent != NULL) delete_case2(n);
}


void
delete_case2(struct node *n)
{
	struct node *s = sibling(n);

	if (s->color == RED) {
		n->parent->color = RED;
		s->color = BLACK;
		if (n == n->parent->left)
			rotate_left(n->parent);
		else
			rotate_right(n->parent);
	}
	delete_case3(n);
}


void
delete_case3(struct node *n)
{
	struct node *s = sibling(n);

	if ((n->parent->color == BLACK) &&
		(s->color == BLACK) && // s != LEAF because n->parent->color == BLACK
		(s->left == LEAF || s->left->color == BLACK) &&
		(s->right == LEAF || s->right->color == BLACK)) {
		s->color = RED;
		delete_case1(n->parent);
	}
	else
		delete_case4(n);
}

void
delete_case4(struct node *n)
{
	struct node *s = sibling(n);

	if ((n->parent->color == RED) &&
		(s->color == BLACK) &&
		(s->left == LEAF || s->left->color == BLACK) &&
		(s->right == LEAF || s->right->color == BLACK)) {
		s->color = RED;
		n->parent->color = BLACK;
	}
	else
		delete_case5(n);
}

void
delete_case5(struct node *n)
{
	struct node *s = sibling(n);

	if ((n == n->parent->left) && (s->right == LEAF || s->right->color == BLACK)) { 
		s->color = RED;
		s->left->color = BLACK;
		rotate_right(s);
	}
	else if ((n == n->parent->right) && (s->left == LEAF || s->left->color == BLACK)) {
		s->color = RED;
		s->right->color = BLACK;
		rotate_left(s);
	}
	delete_case6(n);
}


void
delete_case6(struct node *n)
{
	struct node *s = sibling(n);

	s->color = n->parent->color;
	n->parent->color = BLACK;

	if (n == n->parent->left) {
		s->right->color = BLACK;
		rotate_left(n->parent);
	}
	else {
		s->left->color = BLACK;
		rotate_right(n->parent);
	}
}
