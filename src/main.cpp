#include "sauc.h"
#include <stdio.h>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <vector>


struct entry
{
	double score;
	bool label;
};

struct result
{
	double auc;
	uint32_t list_size;
};

typedef std::vector<entry> stream;
typedef std::vector<result> resultvector;


uint32_t countlines(const char *name);

uint32_t
countlines(const char *name)
{
	uint32_t num = 0;
	std::ifstream f(name);
	std::string line;

	while (std::getline(f, line)) num++;

	return num;
}


void
read_stream(const char *name, stream & out)
{
	out.resize(countlines(name));
	std::ifstream f(name);

	for (uint32_t i = 0; i < out.size(); i++) {
		uint32_t label;
		f >> out[i].score >> label;
		out[i].label = label > 0;
	}
}



int
main(int argc, char **argv)
{
	static struct option longopts[] = {
		{"out",             required_argument,  NULL, 'o'},
		{"in",              required_argument,  NULL, 'i'},
		{"epsilon",         required_argument,  NULL, 'e'},
		{"window"  ,        required_argument,  NULL, 'w'},
		{"help",            no_argument,        NULL, 'h'},
		{ NULL,             0,                  NULL,  0 }
	};

	char *inname = NULL;
	char *outname = NULL;
	double epsilon = 0;
	uint32_t window = 0;


	int ch;
	while ((ch = getopt_long(argc, argv, "o:i:w:e:h", longopts, NULL)) != -1) {
		switch (ch) {
			case 'h':
				printf("Usage: %s -i <input file> -o <output file> -w <window size> -e <epsilon> [-h]\n", argv[0]);
				printf("  -h    print this help\n");
				printf("  -i    input file\n");
				printf("  -o    output file\n");
				printf("  -w    window size\n");
				printf("  -e    epsilon approximation guarantee\n");
				return 0;
				break;
			case 'i':
				inname = optarg;
				break;
			case 'o':
				outname = optarg;
				break;
			case 'w':
				window = atoi(optarg);
				break;
			case 'e':
				epsilon = atof(optarg);
				break;
		}
	}

	if (inname == NULL) {
		fprintf(stderr, "No input file\n");
		return 1;
	}

	stream entries;
	read_stream(inname, entries);

	resultvector results(entries.size());


	sauc est(1 + epsilon);

	for (uint32_t i = 0; i < entries.size(); i++) {
		if (window > 0 && i >= window)
			est.remove(-entries[i - window].score, entries[i - window].label);
		est.insert(-entries[i].score, entries[i].label); // reverse the score, since the sort should be descending
		results[i].auc = est.estimate();
		results[i].list_size = est.list_size();
	}
	
	FILE *outf = stdout;
	if (outname != NULL)
		outf = fopen(outname, "w");


	for (uint32_t i = 0; i < results.size(); i++) {
		fprintf(outf, "%f %d\n", results[i].auc, results[i].list_size);
	}

	if (outname != NULL) fclose(outf);
		

	//printf("%f\n", est.estimate());

	
	return 0;
}
